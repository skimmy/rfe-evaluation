from collections import namedtuple

SILENT_VERBOSITY = 0
DEBUG_VERBOSITY = 3
ALPHA_DEFAULT = 1e-9

ExperimentalSetup = namedtuple(
    "ExperimentalSetup",
    "model X y predictor",
)

ExperimentResult = namedtuple(
    "ExperimentResult",
    "models best scores cv_scores"
)
