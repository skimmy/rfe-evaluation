from copy import deepcopy

import numpy as np
import numpy.ma as ma

from sklearn.pipeline import make_pipeline
from sklearn.model_selection import cross_validate

from common import *
import model as md
from rand import get_k_fold


def remove_smallest_coefficient(pipeline, X=None, y=None):
    if hasattr(pipeline[-1], "coef_"):
        return np.argmin(np.abs(pipeline[-1].coef_))
    if hasattr(pipeline[-1], "feature_importances_"):
        return np.argmin(np.abs(pipeline[-1].feature_importances_))
    return None


def remove_least_correlated(pipeline, X=None, y=None):
    features = pipeline[0].transformers_[0][2]
    corr = [np.corrcoef(X[:, features[i]], y)[0, 1] for i in range(len(features))]
    return np.argmin(np.abs(corr))


def remove_best(pipeline, X, y, mode="train", cv=5):
    features = pipeline[0].transformers_[0][2]
    model = pipeline[-1]
    if len(features) <= 1:
        return 0
    scores = np.zeros(len(features))
    for i in range(len(features)):
        pipeline = make_pipeline(
            md.make_column_selector(features=np.delete(features, i)),
            model,
        )
        if mode.lower() == "train":
            scores[i] = pipeline.fit(X, y).score(X, y)
        if mode.lower() == "cv":
            cv_result = cross_validate(pipeline, X, y, cv=cv, scoring="neg_mean_squared_error")
            scores[i] = cv_result["test_score"].mean()
    return np.argmax(scores)


def backward_elimination(X, y, model, cv=5, criterion="be-small-coeff") -> ExperimentResult:
    M = X.shape[1]
    features = np.arange(M)
    models = []
    scores = []
    cv_scores = []
    k_fold = get_k_fold(cv)
    for i in range(M):
        # Fit
        pipeline = make_pipeline(
            md.make_column_selector(features=features),
            model,
        )
        pipeline.fit(X, y)
        cv_result = cross_validate(pipeline, X, y, cv=k_fold, scoring="neg_mean_squared_error")
        # Save results before changing the model
        scores.append(pipeline.score(X, y))
        models.append(deepcopy(pipeline))
        models[-1][-1].selected_ = np.copy(features)
        cv_scores.append(cv_result["test_score"])
        # Select removed feature
        to_remove = remove_smallest_coefficient(pipeline)
        if criterion == "be-best-cv":
            to_remove = remove_best(pipeline, X, y, mode="cv", cv=k_fold)
        if criterion == "be-least-corr":
            to_remove = remove_least_correlated(pipeline, X, y)
        if criterion == "be-best-train:":
            to_remove = remove_best(pipeline, X, y, mode="train")
        # Perform the actual elimination
        features = np.delete(features, to_remove)
    return ExperimentResult(
        models=models,
        best=np.argmax(scores),
        scores=scores,
        cv_scores=cv_scores,
    )


def add_most_correlated(features, X, y):
    corr = [np.corrcoef(X[:, features[i]], y)[0, 1] for i in range(len(features))]
    return np.argmax(np.abs(corr))


def add_largest_coefficient(model, selected, unselected, X, y):
    pipeline = make_pipeline(
        md.make_column_selector(selected),
        model,
    )
    betas = np.zeros(len(unselected))
    y_prime = y
    if len(selected) > 0:
        y_prime = y - pipeline.fit(X, y).predict(X)
    for i in range(len(unselected)):
        pipeline = make_pipeline(
            md.make_column_selector([unselected[i]]),
            model,
        ).fit(X, y_prime)
        if hasattr(pipeline[-1], "coef_"):
            betas[i] = np.abs(pipeline[-1].coef_[0])
        if hasattr(pipeline[-1], "feature_importances_"):
            betas[i] = pipeline[-1].feature_importances_[0]
    return np.argmax(betas)


def add_best(model, selected, unselected, X, y, mode="train", cv=5):
    scores = np.zeros(len(unselected))
    for i in range(len(unselected)):
        pipeline = make_pipeline(
            md.make_column_selector(np.append(selected, unselected[i])),
            model,
        )
        if mode.lower() == "train":
            scores[i] = pipeline.fit(X, y).score(X, y)
        if mode.lower() == "cv":
            cv_result = cross_validate(pipeline, X, y, cv=cv, scoring="neg_mean_squared_error")
            scores[i] = cv_result["test_score"].mean()
    return np.argmax(scores)


def forward_selection(X, y, model, cv=5, criterion="fs-large-coeff") -> ExperimentResult:
    M = X.shape[1]
    unselected = np.arange(M)
    selected = np.asarray([], dtype=int)
    models = []
    scores = []
    cv_scores = []
    k_fold = get_k_fold(cv)
    for i in range(M):
        # Select next feature to add
        i_add = -1
        if criterion == "fs-most-corr":
            i_add = add_most_correlated(unselected, X, y)
        if criterion == "fs-large-coeff":
            i_add = add_largest_coefficient(model, selected, unselected, X, y)
        if criterion == "fs-best-cv":
            i_add = add_best(model, selected, unselected, X, y, mode="cv", cv=k_fold)
        if criterion == "fs-best-train":
            i_add = add_best(model, selected, unselected, X, y, mode="train")
        selected = np.append(selected, unselected[i_add])
        unselected = np.delete(unselected, i_add)
        # Build and fit model
        pipeline = make_pipeline(
            md.make_column_selector(features=selected),
            model,
        )
        pipeline.fit(X, y)
        # Save results
        cv_result = cross_validate(pipeline, X, y, cv=k_fold, scoring="neg_mean_squared_error")
        # Save results before changing the model
        scores.append(pipeline.score(X, y))
        models.append(deepcopy(pipeline))
        models[-1][-1].selected_ = np.copy(selected)
        cv_scores.append(cv_result["test_score"])
    return ExperimentResult(
        models=models,
        best=np.argmax(scores),
        scores=scores,
        cv_scores=cv_scores,
    )


def shrinkage_method(X, y, cv=5, criterion="sm-lasso", n_alphas=100) -> ExperimentResult:
    features = np.arange(X.shape[1])
    alphas = np.linspace(1e-4, 10, n_alphas)
    models = []
    scores = []
    cv_scores = []
    k_fold = get_k_fold(cv)
    for i in range(n_alphas):
        alpha = alphas[i]
        if criterion.lower() == "sm-lasso":
            pipeline = make_pipeline(
                md.make_lasso(alpha=alpha)
            )
        if criterion.lower() == "sm-enet":
            pipeline = make_pipeline(
                md.make_elastic_net(alpha=alpha)
            )
        pipeline.fit(X, y)
        cv_result = cross_validate(pipeline, X, y, cv=k_fold, scoring="neg_mean_squared_error")
        scores.append(pipeline.score(X, y))
        mask = ma.masked_values(pipeline[-1].coef_, 0).mask
        # Use flatten for cases mask is False and not an array
        selected = features[~mask].flatten()
        models.append(deepcopy(pipeline))
        models[-1][-1].selected_ = np.copy(selected)
        cv_scores.append(cv_result["test_score"])
    return ExperimentResult(
        models=models,
        best=np.argmax(scores),
        scores=scores,
        cv_scores=cv_scores,
    )
