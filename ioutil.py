import os
import pickle

import pandas as pd


def results_to_file(all_results, file_path, args) -> pd.DataFrame:
    df = pd.DataFrame(columns=all_results.keys())
    for k in all_results.keys():
        result = all_results[k]
        for metric in result.keys():
            df.at[metric, k] = result[metric]
        for arg in args.keys():
            df.at[arg, k] = args[arg]
    _, extension = os.path.splitext(file_path)
    if extension.lower() == ".csv":
        df.to_csv(file_path)
    elif extension.lower() == ".json":
        df.to_json(file_path)
    return df


def dump_all(dump_file, args, setup, results):
    with open(dump_file, "wb") as f:
        to_dump = {
            "args": args,
            "setup": setup,
            "results": results,
        }
        pickle.dump(to_dump, f)


def print_results(all_results):
    for k in all_results.keys():
        result = all_results[k]
        print()
        print(k)
        print()
        print(" True: ", result["mse_true"])
        print(" Pred: ", result["mse_estimate"])
        print(" Diff: ", result["mse_diff"])
        print()
        print(" # All   ", result["all"])
        print(" # True  ", result["true"])
        print(" # Pred  ", result["selected"])
        print(" TP      ", result["tp"])
        print(" TN      ", result["tn"])
        print(" FP      ", result["fp"])
        print(" FN      ", result["fn"])
