import numpy as np

from common import *
import model as md
from opts import parse_arguments
from data import multivariate_features, prediction_from_model, make_covariance_matrix
from selection import forward_selection, backward_elimination, shrinkage_method
from eval import compare_mse, compare_mse_file, compare_selected
import ioutil
from rand import global_seeding
import features as ft


def prepare_simulation(args) -> ExperimentalSetup:
    # Make model, data, and predictor
    if (args.x_file is not None) and (args.y_file is not None):
        X = np.loadtxt(args.x_file, delimiter=",")
        y = np.loadtxt(args.y_file, delimiter=",")
        model = md.FileModel()
    else:
        # Generate features:
        #  n_features:    total number of features informative + non-informative
        #  n_significant: number of informative features
        #  n_independent: number of uncorrelated features
        #  n_collinear:   number of non-informative features correlated with others = n_f - (n_s + n_i)
        feature_mask = ft.make_features_mask(
            args.n_significant,
            args.n_independent,
            args.n_features - (args.n_significant + args.n_independent),
        )
        cov_matrix = make_covariance_matrix(feature_mask, args.covariance, (args.feature_std ** 2))
        mean_array = np.ones(args.n_features) * args.feature_mean
        model = md.make_model(feature_mask, args.gen_model.lower())
        X = multivariate_features(args.n_samples, mean_array, cov_matrix)
        y = prediction_from_model(X, model, args.noise_mean, args.noise_std)

    # Crete predictor model
    if args.predictor.lower() == "rf":
        predictor = md.make_random_forest()
    else:
        predictor = md.make_ridge()

    return ExperimentalSetup(model=model, X=X, y=y, predictor=predictor)


def simulation(exp_setup: ExperimentalSetup, selection_method):
    # TODO Make a dictionary of lists with key the selection method and value the list of criteria (add 'all')
    exp_results = {}
    # Forward Selection (FS)
    if selection_method.lower() == "fs":
        for criterion in ["fs-large-coeff", "fs-most-corr", "fs-best-train", "fs-best-cv"]:
            exp_results[criterion] = forward_selection(
                exp_setup.X,
                exp_setup.y,
                exp_setup.predictor,
                criterion=criterion
            )
    # Backward Elimination (BE)
    if selection_method.lower() == "be":
        for criterion in ["be-small-coeff", "be-least-corr", "be-best-train", "be-best-cv"]:
            exp_results[criterion] = backward_elimination(
                exp_setup.X,
                exp_setup.y,
                exp_setup.predictor,
                criterion=criterion
            )
    # Shrinkage Method (SM)
    if selection_method.lower() == "sm":
        for criterion in ["sm-lasso", "sm-enet"]:
            exp_results[criterion] = shrinkage_method(
                exp_setup.X,
                exp_setup.y,
                criterion=criterion
            )
    return exp_results


def build_results(setup: ExperimentalSetup, results):
    all_results = {}

    if isinstance(setup.model, md.FileModel):
        for k in results:
            result = results[k]
            cv_best = np.argmax(np.asarray(result.cv_scores).mean(1))
            model_best = result.models[cv_best]
            mse_result = compare_mse_file(setup.X, setup.y, model_best)
            all_results[k] = {**mse_result}
    else:
        for k in results:
            result = results[k]

            cv_best = np.argmax(np.asarray(result.cv_scores).mean(1))
            model_best = result.models[cv_best]
            # Compare Error and Score
            mse_result = compare_mse(setup.X, setup.y, setup.model[-1], model_best)
            # Compare coefficients
            pred_selected = model_best[-1].selected_
            selection_result = compare_selected(
                setup.model[-1].significant_,
                pred_selected,
                setup.model[-1].all_
            )
            all_results[k] = {**mse_result, **selection_result}
    return all_results


def main():
    args = parse_arguments()
    if args.verbosity == DEBUG_VERBOSITY:
        print()
        print("Args:", args)
    global_seeding(args.seed)
    # Prepare experimental setup
    setup = prepare_simulation(args)
    # Simulation
    results = simulation(setup, args.selection_method)
    # Save output
    all_result = build_results(setup, results)
    if args.out_file is not None:
        ioutil.results_to_file(all_result, args.out_file, vars(args))
    if args.dump_file is not None:
        ioutil.dump_all(args.dump_file, args, setup, results)
    if args.verbosity > SILENT_VERBOSITY:
        ioutil.print_results(all_result)


def test():
    import features
    import model
    import data
    mask = features.make_features_mask(3, 1, 2)
    gen_model = model.make_model(mask, model="lf")[-1]
    print(gen_model.feature_mask_)
    print(gen_model.coef_)
    print(gen_model.intercept_)
    X = np.random.randn(10, len(mask))
    print(gen_model.predict(X))
    cm = make_covariance_matrix(mask, 0.5)
    print(cm)
    mean = 1
    X = data.multivariate_features(1000, np.ones(len(mask)) * mean, cm).T
    est_cov = np.cov(X)
    print(est_cov)


if __name__ == "__main__":
    main()
    # test()
