import numpy as np

from sklearn.model_selection import KFold


def global_seeding(seed):
    if seed is not None:
        np.random.seed(seed)


def random_choice(v, n_choices):
    return np.random.choice(v, size=n_choices, replace=False)


def normal(n_points, mean=0, std=1):
    return mean + np.random.randn(n_points) / std


def shuffle_inplace(a):
    np.random.shuffle(a)
    return a


def get_k_fold(k):
    return KFold(n_splits=k, shuffle=True)
