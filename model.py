import numpy as np

from sklearn.base import BaseEstimator
from sklearn.preprocessing import PolynomialFeatures
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import make_pipeline
from sklearn.dummy import DummyRegressor
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.linear_model import ElasticNet
from sklearn.ensemble import RandomForestRegressor

from common import *
import rand
import features as ft


class FileModel(BaseEstimator):
    def __init__(self):
        pass

    def fit(self, X, y=None):
        pass

    def predict(self, X):
        pass


class CoxModel(BaseEstimator):
    def __init__(self, lambda_=0.5):
        self.lambda_ = lambda_
        self.coef_ = None
        self.intercept_ = 0

    def fit(self, X, y=None):
        self.coef_ = np.zeros(X.shape[1]) + self.intercept_

    def predict(self, X):
        return self.lambda_ * np.exp(np.matmul(X, self.coef_))


def make_polynomial_features(degree=2, include_bias=False) -> PolynomialFeatures:
    return PolynomialFeatures(degree=degree, include_bias=include_bias)


def make_column_selector(features) -> ColumnTransformer:
    return ColumnTransformer(transformers=[("ct", "passthrough", features)])


def make_ridge(fit_intercept=True, alpha=ALPHA_DEFAULT) -> Ridge:
    return Ridge(fit_intercept=fit_intercept, alpha=alpha)


def make_lasso(fit_intercept=True, alpha=None) -> Lasso:
    return Lasso(fit_intercept=fit_intercept, alpha=alpha)


def make_elastic_net(fit_intercept=True, alpha=None) -> ElasticNet:
    return ElasticNet(fit_intercept=fit_intercept, alpha=alpha)


def make_random_forest() -> RandomForestRegressor:
    return RandomForestRegressor(n_estimators=20)


def make_cox(lambda_=0.5) -> CoxModel:
    return CoxModel(lambda_)


def dummy_zero():
    return DummyRegressor(strategy="constant", constant=0)


def set_significant_coefficients(model, features_mask, make_intercept=True):
    n_significant = ft.count_significant(features_mask)
    mask = ft.mask_significant(features_mask)
    coeff_significant = rand.normal(n_significant)
    model.coef_[mask] = coeff_significant
    model.feature_mask_ = features_mask
    if make_intercept:
        model.intercept_ = rand.normal(1)[0]
    model.all_ = np.arange(len(features_mask))
    model.significant_ = ft.list_significant(features_mask)


def make_model(features_mask, model='ridge'):
    m = len(features_mask)
    pipeline = None
    if model == 'lf':
        pipeline = make_pipeline(
            make_ridge()
        )
    if model == 'cox':
        pipeline = make_pipeline(
            make_cox()
        )
    if pipeline is None:
        raise ValueError("Unrecognized model " + model)
    # make a dummy training with one point and "reset it"
    pipeline.fit(
        X=np.random.randn(1, m).reshape(1, m),
        y=np.random.randn(1)
    )
    # zero all coefficients
    pipeline[-1].coef_ = np.zeros_like(pipeline[-1].coef_)
    pipeline[-1].intercept_ = 0
    set_significant_coefficients(pipeline[-1], features_mask)
    return pipeline
