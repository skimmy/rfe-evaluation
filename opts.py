import argparse
import datetime


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--samples",
        dest="n_samples",
        help="number of samples for the random test data",
        type=int,
        default=100,
    )
    parser.add_argument(
        "--features",
        dest="n_features",
        help="number of 'natural' (untransformed) features",
        type=int,
        default=4
    )
    parser.add_argument(
        "--significant",
        dest="n_significant",
        help="number of significant features",
        type=int,
    )
    parser.add_argument(
        "--independent",
        dest="n_independent",
        help="number of independent (uncorrelated) features (default 0)",
        type=int,
        default=0,
    )
    parser.add_argument(
        "--gen-model",
        dest="gen_model",
        help="model used to generate predicted values (default LF)",
        default="LF",
    )
    parser.add_argument(
        "--noise-mean",
        dest="noise_mean",
        help="Mean of Gaussian noise for y (default 0)",
        type=float,
        default=0.0
    )
    parser.add_argument(
        "--noise-std",
        dest="noise_std",
        help="Standard deviation of Gaussian noise for y (default 1.0)",
        type=float,
        default=1.0
    )
    parser.add_argument(
        "--feature-mean",
        dest="feature_mean",
        help="Mean of all features (default 0)",
        type=float,
        default=0.0,
    )
    parser.add_argument(
        "--feature-std",
        dest="feature_std",
        help="Standard deviation of all features (default 1.0)",
        type=float,
        default=1.0,
    )
    parser.add_argument(
        "--covariance",
        dest="covariance",
        help="Covariance for collinear features (default 0.5)",
        type=float,
        default=0.5
    )
    parser.add_argument(
        "--predictor",
        dest="predictor",
        help="predictor model used: LF (default), RF",
        default="LF",
    )
    parser.add_argument(
        "--selection-method",
        dest="selection_method",
        help="feature selection method: FS (default), BE, SM",
        default="FS",
    )
    parser.add_argument(
        "--seed",
        dest="seed",
        help="if given, set the random seed",
        type=int,
    )
    parser.add_argument(
        "--name",
        dest="name",
        help="assigns a name to the current run",
        default=datetime.datetime.now().strftime("_%Y%m%d_%H%M%S_")
    )
    parser.add_argument(
        "--out-file",
        dest="out_file",
        help="if set save results on given file",
        type=str,
        default=None,
    )
    parser.add_argument(
        "--dump-file",
        dest="dump_file",
        help="file to dump setup and results (default None -> no dump)",
        default=None,
    )
    parser.add_argument(
        "--verbosity",
        dest="verbosity",
        help="verbosity level of console output: 0 (no out) -- 3 (debug)",
        type=int,
        default=1,
    )
    parser.add_argument(
        "--x-file",
        dest="x_file",
        help="input file for features",
        default=None,
    )
    parser.add_argument(
        "--y-file",
        dest="y_file",
        help="input file for predictions",
        default=None,
    )
    return parser.parse_args()
