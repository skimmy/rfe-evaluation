import sys
import os

import pandas as pd

if len(sys.argv) < 3:
    print("\nUsage: {0} dest.csv source.csv".format(sys.argv[0]))
    sys.exit(1)
dest_file = sys.argv[1]
source_file = sys.argv[2]

df_source = pd.read_csv(source_file, index_col=0)

# No dest, just copy source
if not os.path.exists(dest_file):
    df_source.to_csv(dest_file)
    sys.exit(0)

df_dest = pd.read_csv(dest_file, index_col=0)
df_new = pd.concat([df_dest, df_source], axis=1)

df_new.to_csv(dest_file)

