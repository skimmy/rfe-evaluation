import numpy as np

from sklearn.metrics import mean_squared_error


def compare_mse(X, y, true, estimate):
    mse_true = mean_squared_error(true.predict(X), y)
    mse_estimate = mean_squared_error(estimate.predict(X), y)
    mse_diff = np.abs(mse_true - mse_estimate)
    return {
        "mse_true": mse_true,
        "mse_estimate": mse_estimate,
        "mse_diff": mse_diff
    }


def compare_mse_file(X, y, estimate):
    return {
        "mse_estimate": mean_squared_error(estimate.predict(X), y)
    }


def compare_selected(true, estimate, all):
    intersect = np.intersect1d(true, estimate)
    fn = np.setdiff1d(true, estimate)
    tn = np.intersect1d(
        np.setdiff1d(all, true),
        np.setdiff1d(all, estimate)
    )
    return {
        "all": len(all),
        "true": len(true),
        "selected": len(estimate),
        "tp": len(intersect),
        "fp": len(estimate) - len(intersect),
        "fn": len(fn),
        "tn": len(tn),
    }
