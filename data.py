import numpy as np
import features as ft


def from_gaussian_features(n_sample, n_feature, mean=0, std=1):
    return np.random.normal(mean, std, size=(n_sample, n_feature))


def multivariate_features(n_sample, mean_array, covariance_matrix):
    return np.random.multivariate_normal(mean_array, covariance_matrix, n_sample)


def prediction_from_model(X, model, noise_mean=0, noise_std=0):
    return model.predict(X) + np.random.normal(noise_mean, noise_std, X.shape[0])


def make_covariance_matrix(mask, corr=.5, var=1.0):
    m = len(mask)
    C = np.identity(m) * var
    for i in range(m):
        if mask[i] == ft.FeatCol:
            j = ft.random_significant_index(mask)
            C[i, j] = C[j, i] = corr
    return C
