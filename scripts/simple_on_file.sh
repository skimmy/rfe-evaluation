#!/bin/zsh

x_file="./data/residential/X_norm.txt"
y_file="./data/residential/y1.txt"
out_file="/tmp/out_.csv"
dump_file="/tmp/dump_.bin"

method="be"
predictor="LF"

verbosity=0
seed=22

while [[ $# -gt 0 ]]; do
  KEY=$1
  case ${KEY} in
  --x)
    x_file="$2"
    shift
    shift
    ;;
  --y)
    y_file="$2"
    shift
    shift
    ;;
  --method)
    method="$2"
    shift
    shift
    ;;
  --predictor)
    predictor="$2"
    shift
    shift
    ;;
  --seed)
    seed="$2"
    shift
    shift
    ;;
  --out_file)
    out_file="$2"
    shift
    shift
    ;;
  --dump_file)
    dump_file="$2"
    shift
    shift
    ;;
  esac
done

eval "python main.py --x-file ${x_file} --y-file ${y_file} --out-file ${out_file} --dump-file ${dump_file}\
  --predictor ${predictor} --selection-method ${method} --verbosity ${verbosity}"