#!/bin/zsh

name_opt=""
seed_opt=""
dump_dir=""

out_dir="/tmp"
x_file="X.txt"
y_file="y.txt"

while [[ $# -gt 0 ]]; do
  KEY=$1
  case ${KEY} in
  --out-dir)
    out_dir="$2"
    shift
    shift
    ;;
  --dump-dir)
    dump_dir="$2"
    shift
    shift
    ;;
  --seed)
    seed_opt="--seed $2"
    shift
    shift
    ;;
  --x-file)
    x_file="$2"
    shift
    shift
    ;;
  --y-file)
    y_file="$2"
    shift
    shift
    ;;
  esac
done

result_file="${out_dir}/results.csv"

for method in fs be sm; do
  out_file="${out_dir}/${method}__.csv"
  dump_file="${out_dir}/${method}__.bin"
  eval "python main.py --x-file ${x_file} --y-file ${y_file} --selection-method ${method}\
    --out-file ${out_file} ${name_opt} ${seed_opt} --dump-file ${dump_file} --verbosity 0"

  eval "python postprocessing/result_merge_columns.py ${result_file} ${out_file}"

  rm -f "${out_file}"
  if [ -z "${dump_dir}" ]; then
    rm "${dump_file}"
  else
    mv "${dump_file}" "${dump_dir}/${method}_dump.bin"
  fi
done