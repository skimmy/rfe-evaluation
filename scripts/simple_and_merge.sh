#!/bin/zsh

# Run one setup with all FS methods (FS, BE, SM) and merge results in single file

n_features=10
n_significant=5
n_independent=0
n_samples=100

y_noise_mean=0
y_noise_std=1
x_noise_mean=0
x_noise_std=1
x_noise_cov=0.5

gen_model=LF
predictor=LF

name_opt=""
seed_opt=""
dump_dir=""

out_dir="/tmp"

while [[ $# -gt 0 ]]; do
  KEY=$1
  case ${KEY} in
  --samples)
    n_samples="$2"
    shift
    shift
    ;;
  --features)
    n_features="$2"
    shift
    shift
    ;;
  --significant)
    n_significant="$2"
    shift
    shift
    ;;
  --independent)
    n_independent="$2"
    shift
    shift
    ;;
  --gen-model)
    gen_model="$2"
    shift
    shift
    ;;
  --predictor)
    predictor="$2"
    shift
    shift
    ;;
  --noise-std)
    y_noise_std="$2"
    shift
    shift
    ;;
  --covariance)
    x_noise_cov="$2"
    shift
    shift
    ;;
  --feature-mean)
    x_noise_mean="$2"
    shift
    shift
    ;;
  --feature-std)
    x_noise_std="$2"
    shift
    shift
    ;;
  --seed)
    seed_opt="--seed $2"
    shift
    shift
    ;;
  --out-dir)
    out_dir="$2"
    shift
    shift
    ;;
  --dump-dir)
    dump_dir="$2"
    shift
    shift
    ;;
  esac
done

result_file="${out_dir}/results.csv"

for method in fs be sm; do
  out_file="${out_dir}/${method}__.csv"
  dump_file="${out_dir}/${method}__.bin"
  eval "python main.py --samples ${n_samples} --features ${n_features} --significant ${n_significant} --independent ${n_independent}\
    --gen-model ${gen_model} --predictor ${predictor} --selection-method ${method}\
    --noise-mean ${y_noise_mean} --noise-std ${y_noise_std} --feature-mean ${x_noise_mean} --feature-std ${x_noise_std} --covariance ${x_noise_cov}\
    --out-file ${out_file} ${name_opt} ${seed_opt} --dump-file ${dump_file}"
  eval "python postprocessing/result_merge_columns.py ${result_file} ${out_file}"
  rm -f "${out_file}"
  if [ -z "${dump_dir}" ]; then
    rm "${dump_file}"
  else
    mv "${dump_file}" "${dump_dir}/${method}_dump.bin"
  fi
done
