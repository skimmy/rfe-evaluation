import numpy as np
import numpy.ma as ma
from rand import shuffle_inplace, random_choice

FeatSign = 0
FeatInd = 1
FeatCol = 2


def make_features_mask(n_significant, n_independent, n_collinear):
    mask = np.asarray([FeatSign] * n_significant + [FeatInd] * n_independent + [FeatCol] * n_collinear)
    return shuffle_inplace(mask)


def count_significant(features_mask):
    return np.sum(np.asarray(features_mask) == FeatSign)


def count_independent(features_mask):
    return np.sum(np.asarray(features_mask) == FeatInd)


def count_collinear(features_mask):
    return np.sum(np.asarray(features_mask) == FeatCol)


def mask_significant(features_mask):
    return ma.masked_equal(features_mask, FeatSign).mask


def mask_independent(features_mask):
    return ma.masked_equal(features_mask, FeatInd).mask


def mask_collinear(features_mask):
    return ma.masked_equal(features_mask, FeatCol).mask


def list_significant(features_mask):
    return np.arange(len(features_mask))[mask_significant(features_mask)]


def list_independent(features_mask):
    return np.arange(len(features_mask))[mask_independent(features_mask)]


def list_collinear(features_mask):
    return np.arange(len(features_mask))[mask_collinear(features_mask)]


def random_significant_index(features_mask):
    mask = ma.masked_equal(features_mask, FeatSign).mask
    return random_choice(np.arange(len(features_mask))[mask], 1)


def random_independent_index(features_mask):
    mask = ma.masked_equal(features_mask, FeatInd).mask
    return random_choice(np.arange(len(features_mask))[mask], 1)


def random_collinear_index(features_mask):
    mask = ma.masked_equal(features_mask, FeatCol).mask
    return random_choice(np.arange(len(features_mask))[mask], 1)
